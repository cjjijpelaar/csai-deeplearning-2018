﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace DeepLearningPreprocessDataset
{
    class Program
    {
        static void Main(string[] args)
        {
            DirectoryInfo directory = new DirectoryInfo(@"D:\niels\Downloads\Dataset\images");
            
            if (directory != null)
            {
                FileInfo[] allImages = directory.GetFiles("*.png");

                List<string> digitLabels = new List<string>();
                List<string> alphabetLabels = new List<string>();

                try
                {
                    Console.Write("Working");
                    foreach (FileInfo file in allImages)
                    {
                        Console.Write(".");

                        // Split filename
                        string[] filenameParts = file.Name.Split('_');

                        string subDir;
                        if (int.TryParse(filenameParts[1], out int number))
                        {
                            // Store label
                            digitLabels.Add(filenameParts[1]);
                            subDir = "digits";
                        }
                        else
                        {
                            // Store label
                            alphabetLabels.Add(filenameParts[1]);
                            subDir = "alphabet";
                        }

                        // Load image
                        Image image = Image.FromFile(file.FullName, true);

                        // Create new one
                        Image newImage = FixedSize(image, 200, 200);

                        // Store image
                        newImage.Save(@"D:\niels\Downloads\Dataset\preprocessed\" + subDir + @"\" + file.Name);
                    }

                    // Store labels
                    File.WriteAllLines(@"D:\niels\Downloads\Dataset\preprocessed\digits\labels.txt", digitLabels);
                    File.WriteAllLines(@"D:\niels\Downloads\Dataset\preprocessed\alphabet\labels.txt", alphabetLabels);

                    Console.WriteLine("\nDone!");

                }
                catch (Exception)
                {
                    Console.WriteLine("\nSomething went wrong!");
                }

                Console.WriteLine("\nPress any key to exit!");
                Console.ReadKey();
            }

        }

        static Image FixedSize(Image imgPhoto, int width, int height)
        {
            int sourceWidth = imgPhoto.Width;
            int sourceHeight = imgPhoto.Height;
            int sourceX = 0;
            int sourceY = 0;
            int destX = 0;
            int destY = 0;

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = (width / (float)sourceWidth);
            nPercentH = (height / (float)sourceHeight);

            if (nPercentH < nPercentW)
            {
                nPercent = nPercentH;
                destX = Convert.ToInt16((width - (sourceWidth * nPercent)) / 2);
            }
            else
            {
                nPercent = nPercentW;
                destY = Convert.ToInt16((height - (sourceHeight * nPercent)) / 2);
            }

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap bmPhoto = new Bitmap(width, height, PixelFormat.Format24bppRgb);

            bmPhoto.SetResolution(imgPhoto.HorizontalResolution, imgPhoto.VerticalResolution);

            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.Clear(Color.Black);
            grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;

            grPhoto.DrawImage(imgPhoto, new Rectangle(destX, destY, destWidth, destHeight),  new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight), GraphicsUnit.Pixel);

            grPhoto.Dispose();

            return bmPhoto;
        }
    }


}
